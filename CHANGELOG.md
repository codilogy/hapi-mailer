# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.1.3"></a>
## [0.1.3](https://bitbucket.org/codilogy/hapi-mailer/compare/v0.1.2...v0.1.3) (2017-07-01)



<a name="0.1.2"></a>
## [0.1.2](https://bitbucket.org/codilogy/hapi-mailer/compare/v0.1.1...v0.1.2) (2017-06-20)



<a name="0.1.1"></a>
## [0.1.1](https://bitbucket.org/codilogy/hapi-mailer/compare/v0.1.0...v0.1.1) (2017-06-20)



<a name="0.1.0"></a>
# 0.1.0 (2017-04-30)


### Features

* initial commit ([d448ecb](https://bitbucket.org/codilogy/hapi-mailer/commits/d448ecb))
