const Promise = require('bluebird')
const bunyanAdapter = require('./bunyan-adapter')
const transportVerify = require('../lib/transport-verify-handler')

module.exports = function nodemailerAdapter (server, nodemailer) {
  const transports = new Map()
  const logger = server.log.bind(server)

  const createTransport = (config) => {
    if (config.settings.logger === true) {
      config.settings.logger = bunyanAdapter(logger)
    } else {
      config.settings.logger = false
    }

    transports.set(config.name, nodemailer.createTransport(config.settings, config.defaults))
    transports.get(config.name).verify(transportVerify(config, logger))
  }

  const getTransport = (name) => transports.get(name)

  const registerTransports = (transports) => Object
    .keys(transports)
    .map(name => Object.assign({}, transports[name], {name}))
    .map(conf => createTransport(conf))

  const sendMail = (template, options) => {
    return Promise.all([
      server.render(`${template}/text`, options.data),
      server.render(`${template}/html`, options.data)
    ]).spread((text, html) => {
      // TODO: Option to select mailer with fallback to default
      transports.get('default').sendMail({
        // TODO: Support all options available for sendMail
        to: options.email,
        subject: options.subject,
        text,
        html
      })
    }).catch(error => {
      server.log(error, error)
    })
  }

  return {
    createTransport,
    getTransport,
    registerTransports,
    sendMail
  }
}
