module.exports = (config, logger) => (error, success) => {
  if (error) {
    logger(error)
  } else {
    logger(`mailer transport "${config.name}" ready`, { config, success })
  }
}
