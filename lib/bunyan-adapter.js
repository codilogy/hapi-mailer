const { format } = require('util')
const BUNYAN_METHODS = ['trace', 'debug', 'info', 'warn', 'error', 'fatal']

/**
 * Creates object which will mimic Buyan logger.
 * @param loggerFn
 * @returns {*}
 */
function bunyanAdapter (loggerFn) {
  return BUNYAN_METHODS.reduce((adapter, method) => {
    adapter[method] = (data, message, ...params) => {
      loggerFn(format(message, ...params), data)
    }

    return adapter
  }, {})
}

bunyanAdapter.methods = BUNYAN_METHODS

module.exports = bunyanAdapter
