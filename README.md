# hapi-mailer

[![JavaScript Standard Style](https://img.shields.io/badge/code_style-standard-green.svg)](https://standardjs.com/)
[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-green.svg)](https://conventionalcommits.org/)
[![bitHound Overall Score](https://www.bithound.io/bitbucket/codilogy/hapi-mailer/badges/score.svg)](https://www.bithound.io/bitbucket/codilogy/hapi-mailer)
[![Coverage Status](https://coveralls.io/repos/bitbucket/codilogy/hapi-mailer/badge.svg)](https://coveralls.io/bitbucket/codilogy/hapi-mailer)

## Installation

```
npm install --save @codilogy/hapi-mailer
```

## Usage

### Options

```json
{
  "templates": {
    "dir": "/dir/with/templates"
  },
  "transports": {
    "default": {
      "settings": {
        "logger": false
      },
      "defaults": {
      
      }
    }
  }
}
```

### API

#### transport

Allows to get registered transport.

```js
const defaultTransport = server.transport('default')
```

#### sendMail

Sends mail. Returns promise.

```js
server.sendMail('confirmation-mail', {
  email: 'destination@mail.com',
  subject: 'Confirmation mail',
  data: {
    
  }
})
```

## Licence

The MIT License (MIT)

Copyright &copy; 2017 Codilogy, [http://codilogy.com](http://codilogy.com) <info@codilogy.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
