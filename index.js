const nodemailer = require('nodemailer')
const nodemailerAdapter = require('./lib/nodemailer-adapter')

exports.register = (server, options, next) => {
  const adapter = nodemailerAdapter(server, nodemailer)

  return server.register([
    require('vision')
  ]).then(() => {
    if (options.transports) {
      adapter.registerTransports(options.transports)
    }

    server.views({
      // TODO: Configurable templates language
      engines: { ejs: require('ejs') },
      // TODO: Configurable templates folder with support for "per transport" templates
      relativeTo: options.templates.dir
    })

    server.decorate('server', 'getTransport', adapter.getTransport)
    server.decorate('server', 'sendMail', adapter.sendMail)

    // TODO: call next() when all transports are successfully verified
    next()
  }).catch(next)
}

exports.register.attributes = {
  pkg: require('./package.json'),
  once: true
}
