/* eslint-env node, mocha */
/* eslint-disable no-unused-expressions */

const chai = require('chai')
const sinonChai = require('sinon-chai')
const nodemailerAdapter = require('../lib/nodemailer-adapter')
const serverMock = require('./mocks/server')
const nodemailerMock = require('./mocks/nodemailer')
const expect = chai.expect

chai.use(sinonChai)

describe('nodemailer adapter', () => {
  let adapter, server, nodemailer

  beforeEach(() => {
    server = serverMock()
    nodemailer = nodemailerMock()
    adapter = nodemailerAdapter(server, nodemailer)
  })

  describe('createTransport', () => {
    it('should create default transport', () => {
      adapter.createTransport({
        name: 'default',
        defaults: {
          from: 'from@mail.com'
        },
        settings: {
          logger: false
        }
      })

      expect(nodemailer.createTransport).to.have.been.calledOnce
    })
  })

  describe('registerTransports', () => {
    it('should register transports defined as object', () => {
      adapter.registerTransports({
        default: {
          defaults: {
            from: 'from@mail.com'
          },
          settings: {
            logger: false
          }
        },
        special: {
          defaults: {
            from: 'special@mail.com'
          },
          settings: {
            logger: true
          }
        }
      })

      expect(nodemailer.createTransport).to.have.been.calledTwice
    })
  })

  describe('getTransport', () => {
    it('should return transport with specified name', () => {
      adapter.createTransport({
        name: 'default',
        defaults: {
          from: 'from@mail.com'
        },
        settings: {
          logger: false
        }
      })

      expect(adapter.getTransport('default')).to.have.deep.property('defaults.from', 'from@mail.com')
      expect(adapter.getTransport('default')).to.have.deep.property('settings.logger', false)
    })
  })

  describe('sendMail', () => {
    beforeEach(() => {
      adapter.registerTransports({
        default: {
          defaults: {
            from: 'from@mail.com'
          },
          settings: {
            logger: false
          }
        },
        special: {
          defaults: {
            from: 'special@mail.com'
          },
          settings: {
            logger: true
          }
        }
      })
    })

    it('should send mail using specified transport', () => {
      adapter.sendMail('confirmation', {
        data: { code: 1 },
        email: 'destination@mail.com',
        subject: 'confirmation mail'
      }).then(() => {
        expect(adapter.getTransport('default').sendMail).to.have.been.calledWith({
          html: 'confirmation/html: {"code":1}',
          subject: 'confirmation mail',
          text: 'confirmation/text: {"code":1}',
          to: 'destination@mail.com'
        })
      })
    })
  })
})
