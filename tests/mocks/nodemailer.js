const sinon = require('sinon')

// const verifyCallback = sinon.stub()
//
// verifyCallback
//   .withArgs('error')
//   .returns()

const transportMock = (settings, defaults) => ({
  settings,
  defaults,
  // verify: sinon.spy(function (fn) {
  //   verifyCallback(fn)
  // }),
  verify: sinon.stub(),
  sendMail: sinon.spy(function (options) {
  })
})

module.exports = () => ({
  createTransport: sinon.spy(function (settings, defaults) {
    return transportMock(settings, defaults)
  })
})
