const sinon = require('sinon')

module.exports = () => ({
  log: sinon.spy(function (message, ...data) {

  }),
  render: sinon.spy(function (template, data) {
    return `${template}: ${JSON.stringify(data)}`
  }),
  register: sinon.spy(function (options) {
    return Promise.resolve()
  }),
  views: sinon.spy(function (options) {
    return Promise.resolve()
  }),
  decorate: sinon.spy(function (ctx, name, handler) {
    return Promise.resolve()
  })
})
