/* eslint-env node, mocha */
/* eslint-disable no-unused-expressions */

const chai = require('chai')
const sinon = require('sinon')
const sinonChai = require('sinon-chai')
const plugin = require('../index')
const serverMock = require('./mocks/server')
const expect = chai.expect

chai.use(sinonChai)

describe('plugin', () => {
  let server, next

  beforeEach(() => {
    next = sinon.spy()
    server = serverMock()

    return plugin.register(server, {
      transports: {
        default: {
          settings: {
            logger: false
          }
        }
      },
      templates: {
        dir: __dirname
      }
    }, next)
  })

  it('should be registered with server', () => {
    expect(server.register).to.have.been.calledWithExactly(sinon.match.array)
  })

  it('should setup views engine', () => {
    expect(server.views).to.have.been.calledOnce
  })

  it('should decorate server with getTransport', () => {
    expect(server.decorate).to.have.been.calledWithExactly('server', 'getTransport', sinon.match.func)
  })

  it('should decorate server with sendMail', () => {
    expect(server.decorate).to.have.been.calledWithExactly('server', 'sendMail', sinon.match.func)
  })

  it('should trigger callback when plugin is ready', () => {
    expect(next).to.have.been.calledOnce
  })
})
