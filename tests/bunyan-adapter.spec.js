/* eslint-env node, mocha */
/* eslint-disable no-unused-expressions */

const { expect } = require('chai')
const bunyanAdapter = require('../lib/bunyan-adapter')
const serverMock = require('./mocks/server')

describe('bunyan adapter', () => {
  let adapter, server

  beforeEach(() => {
    server = serverMock()
    adapter = bunyanAdapter(server.log.bind(server))
  })

  it('should expose bunyan methods', () => {
    expect(bunyanAdapter.methods).to.be.an('array')
  })

  bunyanAdapter.methods.map(method => {
    it(`${method} should call provided logger function with reformatted arguments`, () => {
      adapter[method]({data: 1}, 'message %s %s', 'param 1', 'param 2')

      expect(server.log).to.have.been.calledOnce
      expect(server.log).to.have.been.calledWith('message param 1 param 2', { data: 1 })
    })
  })
})
