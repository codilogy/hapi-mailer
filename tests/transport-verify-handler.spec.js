/* eslint-env node, mocha */

const chai = require('chai')
const sinon = require('sinon')
const sinonChai = require('sinon-chai')
const transportVerify = require('../lib/transport-verify-handler')
const expect = chai.expect

chai.use(sinonChai)

describe('transport verify handler', () => {
  let handler, logger, config

  beforeEach(() => {
    config = {name: 'default'}
    logger = sinon.spy()
    handler = transportVerify(config, logger)
  })

  it('should log error on error', () => {
    const error = new Error('Some error')

    handler(error)
    expect(logger).to.have.been.calledWithExactly(error)
  })

  it('should log confirmation on success', () => {
    handler(null, true)
    expect(logger).to.have.been.calledWithExactly(sinon.match.string, {
      config,
      success: true
    })
  })
})
